import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:score_keeper/common/CustomWidgets.dart';
import 'package:score_keeper/common/Style.dart';
import 'package:score_keeper/simple_game/CustomWidgets.dart';
import 'package:score_keeper/simple_game/Dialogs.dart';
import 'package:score_keeper/simple_game/SimpleGameModel.dart';

class SimpleGameView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => ScopedModelDescendant<SimpleGameModel>(
      builder: (context, _, simpleGameModel) => Scaffold(
          appBar: AppBar(title: Text("Score Keeper")),
          body: Padding(
              padding: EdgeInsets.all(12),
              child: Column(children: [
                Expanded(
                    child: ListView(
                        children: simpleGameModel.currentState.players
                            .map((p) => Dismissible(
                                key: Key(p.uuid),
                                child: SimplePlayerView(p, simpleGameModel),
                                onDismissed: (direction) {
                                  simpleGameModel.removePlayer(p);
                                }))
                            .toList())),
                Row(children: [
                  IconButton(
                    icon: Icon(Icons.undo),
                    onPressed: () => simpleGameModel.undo(),
                  ),
                  Spacer(),
                  SaveSimpleGameButton(
                    height: 30,
                    color: DarkGreen,
                  ),
                  Spacer(),
                  IconButton(
                    icon: Icon(Icons.redo),
                    onPressed: () => simpleGameModel.redo(),
                  )
                ]),
                AddPlayerButton(
                    context: context,
                    height: 65,
                    dialog: AddSimplePlayer(),
                    action: (p) {
                      if (p != null) simpleGameModel.addPlayer(p);
                    })
              ]))));
}

class SimplePlayerView extends StatelessWidget {
  final SimplePlayer _player;
  final SimpleGameModel _model;

  SimplePlayerView(this._player, this._model);

  @override
  Widget build(BuildContext context) => Card(
      child: Padding(
          padding: EdgeInsets.fromLTRB(0, 15, 30, 10),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            ExpandingColumn([
              Text(_player.name,
                  style: TextStyle(fontSize: 33, fontWeight: FontWeight.bold)),
              Text("${_player.score}", style: ScoreStyle(_player.score, 25)),
              Image.asset("resources/icons/${_model.rankOf(_player)}.png",
                  height: 55)
            ]),
            ExpandingColumn([
              ConstrainedBox(
                  constraints: BoxConstraints(minHeight: 0, maxHeight: 180),
                  child: ListView(
                      shrinkWrap: true,
                      children: _player.scores
                          .asMap()
                          .map((i, value) => MapEntry(
                              i,
                              Padding(
                                  padding: EdgeInsets.only(bottom: 8),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("#${i + 1}",
                                            style: TextStyle(
                                                fontSize: 20, color: Grey)),
                                        Text("$value",
                                            style: ScoreStyle(value, 28))
                                      ]))))
                          .entries
                          .map((e) => e.value)
                          .toList())),
              AddScoreButton(
                  context: context,
                  height: 20,
                  dialog: AddScoreSimplePlayer(),
                  action: (v) {
                    if (v != null) _model.addScoreToPlayer(_player, v);
                  })
            ])
          ])));
}
