import 'package:scoped_model/scoped_model.dart';
import 'package:score_keeper/common/HelperMethods.dart';
import 'package:score_keeper/persistence/Persistence.dart';
import 'package:uuid/uuid.dart';

class SimpleGameModel extends Model {
  final List<SimpleGameState> _states;
  final List<SimpleGameState> _redos = [];

  SimpleGameModel({SimpleGameState initState = SimpleGameState.EMPTY})
      : _states = [initState];

  SimpleGameState get currentState => _states.last;

  void addPlayer(SimplePlayer player) => _withNotifyAndPersistence(
      () => _states.add(currentState._addPlayer(player)));

  void removePlayer(SimplePlayer player) => _withNotifyAndPersistence(
      () => _states.add(currentState._removePlayer(player)));

  int rankOf(SimplePlayer player) => currentState._rankOf(player);

  void addScoreToPlayer(SimplePlayer player, int score) =>
      _withNotifyAndPersistence(
          () => _states.add(currentState._addScoreToPlayer(player, score)));

  void _withNotifyAndPersistence(var f, {bool clearRedos = true}) {
    f();
    notifyListeners();
    Persistence.inst.CURRENT_GAME.saveSimpleGame(currentState);
    if(clearRedos) _redos.clear();
  }

  void undo() {
    if (_states.length == 1) return;
    _withNotifyAndPersistence(() {
      _redos.add(_states.last);
      _states.removeLast();
    }, clearRedos: false);
  }

  void redo() {
    if (_redos.length == 0) return;
    _withNotifyAndPersistence(() {
      _states.add(_redos.removeLast());
    }, clearRedos: false);
  }
}

class SimpleGameState {
  final List<SimplePlayer> players;

  const SimpleGameState({this.players});

  static const SimpleGameState EMPTY = SimpleGameState(players: const []);

  SimpleGameState _addPlayer(SimplePlayer player) =>
      SimpleGameState(players: addToList(this.players, player));

  SimpleGameState _removePlayer(SimplePlayer player) => SimpleGameState(
      players: removeWhere(this.players, (p) => p.uuid == player.uuid));

  int _rankOf(SimplePlayer player) {
    List<SimplePlayer> copiedPlayers = players.toList();
    copiedPlayers.sort((a, b) => b.score - a.score);
    return copiedPlayers.indexWhere((p) => p.uuid == player.uuid) + 1;
  }

  SimpleGameState _addScoreToPlayer(SimplePlayer player, int score) {
    int indexOfPlayer = this.players.indexWhere((p) => p.uuid == player.uuid);
    SimplePlayer updatedPlayer =
        this.players.elementAt(indexOfPlayer)._addScore(score);
    return SimpleGameState(
        players: replaceAt(
            list: this.players, index: indexOfPlayer, value: updatedPlayer));
  }
}

class SimplePlayer {
  final String uuid;
  final String name;
  final List<int> scores;

  int get score => scores.fold(0, (a, b) => a + b);

  const SimplePlayer({this.uuid, this.name, this.scores});

  static SimplePlayer createNew(String name) {
    return SimplePlayer(
        uuid: Uuid().v1(), name: name, scores: List.unmodifiable([]));
  }

  SimplePlayer _clone({String name, List<int> scores}) => SimplePlayer(
      uuid: this.uuid,
      name: (name != null) ? name : this.name,
      scores: (scores != null) ? scores : this.scores);

  SimplePlayer _addScore(int score) =>
      this._clone(scores: addToList(this.scores, score));

  SimplePlayer _changeName(String name) => this._clone(name: name);
}
