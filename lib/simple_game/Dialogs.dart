import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:score_keeper/common/CustomWidgets.dart';
import 'package:score_keeper/common/Dialogs.dart';
import 'package:score_keeper/common/Style.dart';
import 'package:score_keeper/simple_game/SimpleGameModel.dart';

class AddSimplePlayer extends StatelessWidget {
  Widget build(BuildContext context) {
    String name = "";
    return PaddedDialog(title: AppLocalizations.of(context).tr('player_name'), children: [
      TextField(
          textCapitalization: TextCapitalization.words,
          textAlign: TextAlign.center,
          keyboardType: TextInputType.text,
          style: TextStyle(fontSize: 30, color: Colors.black),
          autofocus: true,
          onChanged: (value) {
            name = value;
          }),
      ExpandingButton(
          image: "resources/icons/add.png",
          height: 20,
          color: DarkGreen,
          onPressed: () {
            Navigator.pop(context, SimplePlayer.createNew(name));
          })
    ]);
  }
}

class AddScoreSimplePlayer extends StatelessWidget {
  Widget build(BuildContext context) {
    var number = 0;
    return PaddedDialog(title: AppLocalizations.of(context).tr('enter_number'), children: <Widget>[
      TextField(
          textAlign: TextAlign.center,
          keyboardType: TextInputType.number,
          style: TextStyle(fontSize: 30, color: Colors.black),
          autofocus: true,
          onChanged: (value) {
            var parsed = int.tryParse(value);
            parsed == null ? number = 0 : number = parsed;
          }),
      Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.max,
          children: [
            ImageButton(
                image: "resources/icons/sub.png",
                height: 30,
                color: Red,
                onPressed: () {
                  Navigator.pop(context, -number);
                }),
            ImageButton(
                image: "resources/icons/add.png",
                height: 30,
                color: DarkGreen,
                onPressed: () {
                  Navigator.pop(context, number);
                })
          ])
    ]);
  }
}
