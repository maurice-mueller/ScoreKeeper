import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:score_keeper/common/Dialogs.dart';
import 'package:score_keeper/common/GameMetaModel.dart';
import 'package:score_keeper/common/Toaster.dart';
import 'package:score_keeper/persistence/Persistence.dart';
import 'package:score_keeper/simple_game/SimpleGameModel.dart';

class SaveSimpleGameButton extends StatelessWidget {
  double _height;
  Color _color;

  SaveSimpleGameButton({double height, Color color})
      : _height = height,
        _color = color;

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<GameMetaModel>(
        builder: (context, _, metaModel) =>
            ScopedModelDescendant<SimpleGameModel>(
                builder: (context, _, simpleGameModel) {
              return IconButton(
                  icon: Icon(Icons.save, size: 25),
                  color: _color,
                  onPressed: () {
                    if (metaModel.name == null || metaModel.name.isEmpty) {
                      showDialog(
                          context: context,
                          builder: (_) {
                            return SaveGameDialog();
                          }).then((name) {
                        if (name == null || name.isEmpty) return;
                        metaModel.name = name;
                        Toaster.showProcess(
                            i18n: 'game_saving_in_progress',
                            args: [name],
                            context: context);
                        Persistence.inst.SAVED_GAMES
                            .saveSimpleGame(
                                state: simpleGameModel.currentState, name: name)
                            .then((_) {
                          Toaster.showSuccess(
                              i18n: 'game_saved',
                              args: [name],
                              context: context);
                        });
                      });
                      return;
                    }
                    Toaster.showProcess(
                        i18n: 'game_saving_in_progress',
                        args: [metaModel.name],
                        context: context);
                    Persistence.inst.SAVED_GAMES
                        .saveSimpleGame(
                            state: simpleGameModel.currentState,
                            name: metaModel.name)
                        .then((_) {
                      Toaster.showSuccess(
                          i18n: 'game_saved',
                          args: [metaModel.name],
                          context: context);
                    });
                  });
            }));
  }
}
