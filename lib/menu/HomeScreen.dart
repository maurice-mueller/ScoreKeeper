import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:score_keeper/Router.dart';
import 'package:score_keeper/common/Dialogs.dart';
import 'package:score_keeper/common/GameMetaModel.dart';
import 'package:score_keeper/common/RouteArguments.dart';
import 'package:score_keeper/common/Toaster.dart';
import 'package:score_keeper/persistence/Persistence.dart';
import 'package:score_keeper/simple_game/SimpleGameModel.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(AppLocalizations.of(context).tr('title'))),
        body: Center(
          child: ListView(shrinkWrap: true, children: [
            Container(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                    icon: Icon(Icons.play_circle_outline),
                    iconSize: MediaQuery.of(context).size.height * 0.25,
                    color: Theme.of(context).primaryColor,
                    tooltip: AppLocalizations.of(context).tr('new_game'),
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.SIMPLE_GAME_SCREEN,
                          arguments: RouteArguments(
                              model: SimpleGameModel(),
                              metaModel: GameMetaModel()));
                    }),
                Row(
                  children: [
                    Builder(
                      builder: (context) => IconButton(
                          icon: Icon(Icons.cached),
                          iconSize: MediaQuery.of(context).size.height * 0.125,
                          color: Theme.of(context).primaryColor,
                          tooltip: AppLocalizations.of(context)
                              .tr('continue_last_game'),
                          onPressed: () {
                            Persistence.inst.CURRENT_GAME.load().then((v) {
                              if (v == null) {
                                Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text(AppLocalizations.of(context)
                                      .tr('no_current_game')),
                                ));
                                return;
                              }
                              Navigator.pushNamed(
                                  context, Routes.SIMPLE_GAME_SCREEN,
                                  arguments: RouteArguments(
                                      model: SimpleGameModel(initState: v),
                                      metaModel: GameMetaModel()));
                            });
                          }),
                    ),
                    LoadGameButton(),
                    IconButton(
                        icon: Icon(Icons.settings),
                        iconSize: MediaQuery.of(context).size.height * 0.125,
                        color: Theme.of(context).primaryColor,
                        tooltip:
                            AppLocalizations.of(context).tr('settings_title'),
                        onPressed: () {
                          Navigator.pushNamed(context, Routes.SETTINGS_SCREEN);
                        })
                  ],
                  mainAxisAlignment: MainAxisAlignment.center,
                )
              ],
            )),
            IconButton(
                icon: Icon(Icons.info),
                iconSize: MediaQuery.of(context).size.height * 0.125,
                color: Theme.of(context).primaryColor,
                tooltip: AppLocalizations.of(context).tr('about_title'),
                onPressed: () {
                  Navigator.pushNamed(context, Routes.ABOUT_SCREEN);
                }),
          ]),
        ));
  }
}

class LoadGameButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.file_upload),
      iconSize: MediaQuery.of(context).size.height * 0.125,
      color: Theme.of(context).primaryColor,
      tooltip: AppLocalizations.of(context).tr('load_game'),
      onPressed: () {
        Persistence.inst.SAVED_GAMES.allGameNames().then((it) {
          showDialog(
              context: context,
              builder: (_) => LoadGameDialog(allGameNames: it)).then((name) {
            if (name == null) return;
            Toaster.showProcess(
                i18n: 'game_loading_in_progress',
                args: [name],
                context: context);
            Persistence.inst.SAVED_GAMES.load(name).then((state) {
              Toaster.showProcess(
                  i18n: 'game_loaded', args: [name], context: context);
              Navigator.pushNamed(context, Routes.SIMPLE_GAME_SCREEN,
                  arguments: RouteArguments(
                      model: SimpleGameModel(initState: state),
                      metaModel: GameMetaModel(name: name)));
            });
          });
        });
      },
    );
  }
}
