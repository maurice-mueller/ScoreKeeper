import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(AppLocalizations.of(context).tr('about_title'))),
        body: ListView(children: [
          ListTile(
            leading: Icon(Icons.lightbulb_outline, size: 34),
            title: Text('Idea and programming'),
            subtitle: Text('by Maurice Müller')
          ),
          ListTile(
            leading: Icon(Icons.mode_edit, size: 34),
            title: Text('Design and layout'),
            subtitle: Text('by Fabienne Müller')
          )
        ]));
  }
}
