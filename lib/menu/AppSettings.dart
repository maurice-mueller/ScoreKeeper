import 'dart:ui';
import 'package:easy_localization/easy_localization_provider.dart';
import 'package:flutter/widgets.dart';
import 'package:screen/screen.dart';
import 'package:scoped_model/scoped_model.dart';

EasyLocalizationProvider LanguageProvider;

class AppLanguage {
  static const ENGLISH = Locale("en");
  static const GERMAN = Locale("de");

  static const List<Locale> ALL = [ENGLISH, GERMAN];
}

class AppSettings extends Model {
  static AppSettings inst;
  bool _screenAlwaysOn;
  Locale _language;

  AppSettings({bool screenAlwaysOn, Locale language}):
        _screenAlwaysOn = screenAlwaysOn,
        _language= language;


  static void init(BuildContext context) {
     AppSettings.inst = AppSettings._(context: context);
  }

  AppSettings._({BuildContext context})
      : _screenAlwaysOn = false,
        _language = AppLanguage.ENGLISH; // TODO: read from context system language

  Locale get language => _language;

  bool get screenAlwaysOn => _screenAlwaysOn;

  set language(Locale value) =>
      _withNotify(() {
        _language = value;
        LanguageProvider.data.changeLocale(value);
      });

  set screenAlwaysOn(bool value) =>
      _withNotify(() {
        _screenAlwaysOn = value;
        Screen.keepOn(value);
      });

  void _withNotify(var f) {
    f();
    notifyListeners();
  }

  void copyValues(AppSettings settings) {
    this.screenAlwaysOn = settings.screenAlwaysOn;
    this.language = settings.language;
  }

  void toDefault(BuildContext context) {
    this.copyValues(AppSettings._(context: context));
  }
}
