import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:score_keeper/menu/AppSettings.dart';
import 'package:score_keeper/persistence/Persistence.dart';

class AppSettingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AppSettings>(
      builder: (context, _, appSettings) => Scaffold(
      appBar: AppBar(title: Text(AppLocalizations.of(context).tr('settings_title'))),
      body: Column(children: [
        Row(children: [
          Text(AppLocalizations.of(context).tr('settings_screen_always_on')),
          Spacer(),
          Switch(
            value: appSettings.screenAlwaysOn,
            onChanged: (v){
              appSettings.screenAlwaysOn = v;
              Persistence.inst.SETTINGS.save(appSettings);
            } ,
          )
        ]),
        Row(children: [
          Text(AppLocalizations.of(context).tr('settings_language')),
          Spacer(),
          DropdownButton<Locale>(
            items: AppLanguage.ALL.map((Locale value) {
              return new DropdownMenuItem<Locale>(
                value: value,
                child: new Text(value.languageCode),
              );
            }).toList(),
            value: appSettings.language,
            onChanged: (newValue) {
              appSettings.language = newValue;
              Persistence.inst.SETTINGS.save(appSettings);
            },
          )
        ]),
        Spacer(),
        FlatButton(
            color: Theme.of(context).errorColor,
            child: Text(AppLocalizations.of(context).tr('dev_clear_all_data')),
            onPressed: () async {
              Persistence.inst.reinit().then((_) {
                AppSettings.inst.toDefault(context);
                Persistence.inst.SETTINGS.save(AppSettings.inst);
              });
            }),
      ]),
    ));
  }
}
