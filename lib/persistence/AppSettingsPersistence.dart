import 'dart:convert';
import 'dart:ui';

import 'package:score_keeper/menu/AppSettings.dart';
import 'package:sqflite/sqflite.dart';

const _DB_TABLE = 'SETTINGS';
const _DB_TABLE_COLUMN_ID = 'ID';
const _DB_TABLE_COLUMN_CONTENT = 'CONTENT';

class AppSettingsPersistence {
  final Database _database;

  AppSettingsPersistence(this._database);

  Future<AppSettings> load() async {
    List<Map> maps = await _database.rawQuery(
        "SELECT * FROM $_DB_TABLE");
    if (maps.length > 0)
      return _fromJsonString(maps.first[_DB_TABLE_COLUMN_CONTENT]);
    return null;
  }

  Future<int> save(AppSettings settings) async {
    await _database.delete(_DB_TABLE);
    return await _database.insert(_DB_TABLE, _toDatabaseMap(settings));
  }

  Map _toDatabaseMap(AppSettings settings) {
    Map<String, dynamic> map = Map();
    map.putIfAbsent(_DB_TABLE_COLUMN_ID, () => 0);
    map.putIfAbsent(
        _DB_TABLE_COLUMN_CONTENT, () => _toJsonString(settings));
    return map;
  }

  AppSettings _fromJsonString(String jsonString) {
    Map<String, dynamic> json = jsonDecode(jsonString);
    return AppSettings(
        screenAlwaysOn: json['screenAlwaysOn'],
        language: Locale(json['language']));
  }

  String _toJsonString(AppSettings settings) {
    Map<String, dynamic> map = Map();
    map.putIfAbsent('screenAlwaysOn', () => settings.screenAlwaysOn);
    map.putIfAbsent('language', () => settings.language.languageCode);
    return jsonEncode(map);
  }

  static Future<void> createTable(Database database) async {
    await database.execute('''
create table $_DB_TABLE ( 
  $_DB_TABLE_COLUMN_ID integer primary key, 
  $_DB_TABLE_COLUMN_CONTENT text not null)
''');
  }

  Future<void> reinit() async {
    await _database.delete(_DB_TABLE);
  }

}

