import 'dart:convert';
import 'package:score_keeper/simple_game/SimpleGameModel.dart';

String SimpleGameStateToJson(SimpleGameState state) {
  Map<String, dynamic> map = Map();
  map.putIfAbsent(
      'players',
          () => jsonEncode(state.players.map((player) {
        Map<String, dynamic> mapped = Map();
        mapped.putIfAbsent('uuid', () => player.uuid);
        mapped.putIfAbsent('name', () => player.name);
        mapped.putIfAbsent('scores', () => player.scores);
        return mapped;
      }).toList()));
  return jsonEncode(map);
}

SimpleGameState SimpleGameStateFromJson(String jsonString) {
  Map<String, dynamic> json = jsonDecode(jsonString);
  List<dynamic> playersJson = jsonDecode(json['players']);
  List<SimplePlayer> players = playersJson.map((m) {
    List<dynamic> scoresDyn = m['scores'];
    List<int> scores = scoresDyn.map((v) => v as int).toList();
    return SimplePlayer(uuid: m['uuid'], name: m['name'], scores: scores);
  }).toList();
  return SimpleGameState(players: players);
}
