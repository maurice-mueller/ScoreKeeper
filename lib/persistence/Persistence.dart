import 'package:score_keeper/persistence/AppSettingsPersistence.dart';
import 'package:score_keeper/persistence/CurrentGamePersistence.dart';
import 'package:score_keeper/persistence/SavedGamesPersistence.dart';
import 'package:sqflite/sqflite.dart';

const _DB_NAME = 'SCORE_KEEPER';

class Persistence {
  Database _database;
  static Persistence inst;
  AppSettingsPersistence SETTINGS;
  CurrentGamePersistence CURRENT_GAME;
  SavedGamesPersistence SAVED_GAMES;

  Persistence._(this._database,
      {this.SETTINGS, this.CURRENT_GAME, this.SAVED_GAMES}) {
    Persistence.inst = this;
  }

  static Future<Persistence> init() async {
    if (Persistence.inst != null) return Persistence.inst;
    Database database = await openDatabase(_DB_NAME, version: 1,
        onCreate: (Database db, int version) async {
      await AppSettingsPersistence.createTable(db);
      await CurrentGamePersistence.createTable(db);
      await SavedGamesPersistence.createTable(db);
    });
    return Persistence._(database,
        SETTINGS: AppSettingsPersistence(database),
        CURRENT_GAME: CurrentGamePersistence(database),
        SAVED_GAMES: SavedGamesPersistence(database));
  }

  Future<void> reinit() async {
    await SETTINGS.reinit();
    await CURRENT_GAME.reinit();
    await SAVED_GAMES.reinit();
  }
}
