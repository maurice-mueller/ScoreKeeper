import 'package:score_keeper/persistence/PersistenceHelper.dart';
import 'package:score_keeper/simple_game/SimpleGameModel.dart';
import 'package:sqflite/sqflite.dart';

const _DB_TABLE = 'CURRENT_GAME';
const _DB_TABLE_COLUMN_ID = 'ID';
const _DB_TABLE_COLUMN_TYPE = 'TYPE';
const _DB_TABLE_COLUMN_CONTENT = 'CONTENT';

class CurrentGamePersistence {
  final Database _database;

  CurrentGamePersistence(this._database);

  Future<SimpleGameState> load() async {
    List<Map> maps = await _database.rawQuery("SELECT * FROM $_DB_TABLE");
    if (maps.length > 0)
      return SimpleGameStateFromJson(
          maps.first[_DB_TABLE_COLUMN_CONTENT]);
    return null;
  }

  Future<int> saveSimpleGame(SimpleGameState state) async {
    await _database.delete(_DB_TABLE);
    return await _database.insert(
        _DB_TABLE, _toDatabaseMapSimpleGameState(state));
  }

  Map _toDatabaseMapSimpleGameState(SimpleGameState state) {
    Map<String, dynamic> map = Map();
    map.putIfAbsent(_DB_TABLE_COLUMN_ID, () => 0);
    map.putIfAbsent(_DB_TABLE_COLUMN_TYPE, () => 'SIMPLE_GAME');
    map.putIfAbsent(
        _DB_TABLE_COLUMN_CONTENT, () => SimpleGameStateToJson(state));
    return map;
  }

  static Future<void> createTable(Database database) async {
    await database.execute('''
create table $_DB_TABLE ( 
  $_DB_TABLE_COLUMN_ID integer primary key, 
  $_DB_TABLE_COLUMN_TYPE text not null,
  $_DB_TABLE_COLUMN_CONTENT text
  )
''');
  }

  Future<void> reinit() async {
    await _database.delete(_DB_TABLE);
  }
}
