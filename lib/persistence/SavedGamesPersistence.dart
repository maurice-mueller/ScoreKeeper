import 'dart:convert';
import 'package:score_keeper/persistence/PersistenceHelper.dart';
import 'package:score_keeper/simple_game/SimpleGameModel.dart';
import 'package:sqflite/sqflite.dart';

const _DB_TABLE = 'SAVED_GAMES';
const _DB_TABLE_COLUMN_NAME = 'NAME';
const _DB_TABLE_COLUMN_TYPE = 'TYPE';
const _DB_TABLE_COLUMN_CONTENT = 'CONTENT';

class SavedGamesPersistence {
  final Database _database;

  SavedGamesPersistence(this._database);

  Future<List<String>> allGameNames() async {
    List<Map> maps = await _database.rawQuery("SELECT $_DB_TABLE_COLUMN_NAME FROM $_DB_TABLE");
    List<String> mapped = maps.map((value) => value[_DB_TABLE_COLUMN_NAME] as String).toList();
    return mapped;
  }

  Future<SimpleGameState> load(String name) async {
    List<Map> maps = await _database.rawQuery(
        "SELECT * FROM $_DB_TABLE WHERE $_DB_TABLE_COLUMN_NAME='$name'");
    if (maps.length > 0)
      return SimpleGameStateFromJson(
          maps.first[_DB_TABLE_COLUMN_CONTENT]);
    return null;
  }

  Future<int> saveSimpleGame({SimpleGameState state, String name}) async {
    List<Map> maps = await _database.rawQuery(
        "SELECT * FROM $_DB_TABLE WHERE $_DB_TABLE_COLUMN_NAME='$name'");
    if (maps.length > 0) {
      Map currentRow = maps.first;
      Map<String, dynamic> updatedRow = currentRow.map((key, value) {
        if (key == _DB_TABLE_COLUMN_CONTENT)
          return MapEntry(key, SimpleGameStateToJson(state));
        return MapEntry(key, value);
      });
      return await _database.update(_DB_TABLE, updatedRow,
          where: "$_DB_TABLE_COLUMN_NAME=?", whereArgs: [name]);
    }
    return await _database.insert(
        _DB_TABLE, _toDatabaseMapSimpleGameState(state, name));
  }

  Map _toDatabaseMapSimpleGameState(SimpleGameState state, String name) {
    Map<String, dynamic> map = Map();
    map.putIfAbsent(_DB_TABLE_COLUMN_NAME, () => name);
    map.putIfAbsent(_DB_TABLE_COLUMN_TYPE, () => 'SIMPLE_GAME');
    map.putIfAbsent(
        _DB_TABLE_COLUMN_CONTENT, () => SimpleGameStateToJson(state));
    return map;
  }

  static Future<void> createTable(Database database) async {
    await database.execute('''
create table $_DB_TABLE ( 
  $_DB_TABLE_COLUMN_NAME text not null primary key,
  $_DB_TABLE_COLUMN_TYPE text not null,
  $_DB_TABLE_COLUMN_CONTENT text
  )
''');
  }

  Future<void> reinit() async {
    await _database.delete(_DB_TABLE);
  }
}
