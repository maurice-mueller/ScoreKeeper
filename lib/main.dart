import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:score_keeper/Router.dart';
import 'package:score_keeper/common/Style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:score_keeper/menu/AppSettings.dart';
import 'package:score_keeper/persistence/Persistence.dart';

void main() => runApp(ScoreKeeperInit());

class ScoreKeeperInit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AppSettings.init(context);
    Persistence.init().then((_persistence) {
      _initSettingsFromDB(_persistence);
    });
    return ScopedModel<AppSettings>(model: AppSettings.inst, child: EasyLocalization(child:ScoreKeeper()));
  }

  void _initSettingsFromDB(Persistence _persistence) {
    _persistence.SETTINGS.load().then((settings) {
      if (settings == null) {
        print('no settings in DB');
        return;
      }
      print(
          'loaded settings: ${settings.screenAlwaysOn} and ${settings.language.languageCode}');
      AppSettings.inst.copyValues(settings);
    });
  }
}

class ScoreKeeper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LanguageProvider = EasyLocalizationProvider.of(context);
    var easyLocalizationProviderData = LanguageProvider.data;
    return EasyLocalizationProvider(
        data: easyLocalizationProviderData,
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
                primaryColor: DarkGreen,
                accentColor: Orange,
                scaffoldBackgroundColor: VeryLightGreen),
            initialRoute: Routes.HOME_SCREEN,
            routes: Routes.asMap(),
            localizationsDelegates: [
              EasylocaLizationDelegate(
                  locale: easyLocalizationProviderData.locale ?? Locale('en'),
                  path: 'resources/i18n'),
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            supportedLocales: AppLanguage.ALL,
            locale: easyLocalizationProviderData.locale));
  }
}
