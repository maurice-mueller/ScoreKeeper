import 'package:score_keeper/common/GameMetaModel.dart';

class RouteArguments {
  GameMetaModel metaModel;
  Object model;

  RouteArguments({this.metaModel, this.model});

}