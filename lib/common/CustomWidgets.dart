import 'package:flutter/material.dart';
import 'package:score_keeper/common/Style.dart';

class ImageButton extends RaisedButton {
  ImageButton({String image, double height, Color color, var onPressed})
      : super(
            child: Image.asset(image, height: height),
            color: color,
            onPressed: onPressed);
}

class ExpandingButton extends SizedBox {
  ExpandingButton({String image, double height, Color color, var onPressed})
      : super(
            width: double.infinity,
            child: ImageButton(
                image: image, height: height, color: color, onPressed: onPressed));
}

class DialogButton extends ExpandingButton {
  DialogButton(
      {BuildContext context,
      String image,
      double height,
      var dialog,
      var action,
      Color color})
      : super(
            image: image,
            height: height,
            color: color,
            onPressed: () => showDialog(
                context: context,
                builder: (_) {
                  return dialog;
                }).then(action));
}

class ExpandingColumn extends Expanded {
  ExpandingColumn(var c) : super(child: Column(children: c));
}

class AddScoreButton extends DialogButton {
  AddScoreButton({BuildContext context, double height, var dialog, var action})
      : super(
            context: context,
            image: 'resources/icons/add.png',
            height: height,
            dialog: dialog,
            action: action,
            color: LightGreen);
}

class AddPlayerButton extends DialogButton {
  AddPlayerButton({BuildContext context, double height, var dialog, var action})
      : super(
            context: context,
            image: 'resources/icons/addPlayer.png',
            height: height,
            dialog: dialog,
            action: action,
            color: Orange);
}
