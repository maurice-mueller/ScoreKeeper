List<T> addToList<T>(List<T> unmodifiableList, T element) {
  List<T> copy = unmodifiableList.toList();
  copy.add(element);
  return List.unmodifiable(copy);
}

List<T> removeAt<T>(List<T> unmodifiableList, int index) {
  List<T> copy = unmodifiableList.toList();
  copy.removeAt(index);
  return List.unmodifiable(copy);
}

List<T> removeWhere<T>(List<T> unmodifiableList, var f) {
  List<T> copy = unmodifiableList.toList();
  copy.removeWhere(f);
  return List.unmodifiable(copy);
}

List<T> replaceAt<T>({List<T> list, int index, T value}) {
  List<T> copy = list.toList();
  copy.replaceRange(index, index + 1, [value]);
  return List.unmodifiable(copy);
}
