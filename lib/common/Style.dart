
import 'package:flutter/painting.dart';

const VeryLightGreen = Color(0xFFD8EDCE);
const LightGreen = Color(0xFF8CC04C);
const DarkGreen = Color(0xFF196A30);
const Red = Color(0xFFE30613);
const Grey = Color(0xFF575756);
const Orange = Color(0xFFEF7823);

Color ScoreColor({int value}) => value < 0 ? Red : DarkGreen;

TextStyle ScoreStyle(value, double size) => TextStyle(
    color: ScoreColor(value: value),
    fontSize: size,
    fontWeight: FontWeight.bold);
