import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:score_keeper/common/CustomWidgets.dart';
import 'package:score_keeper/common/Style.dart';

class PaddedDialog extends SimpleDialog {
  PaddedDialog({String title, List<Widget> children})
      : super(
            title: Text(title),
            children: children,
            contentPadding: EdgeInsets.all(20));
}

class SaveGameDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String name = "";
    return PaddedDialog(
      title: AppLocalizations.of(context).tr('save_game'),
      children: <Widget>[
        TextField(
            textCapitalization: TextCapitalization.words,
            textAlign: TextAlign.center,
            keyboardType: TextInputType.text,
            style: TextStyle(fontSize: 30, color: Colors.black),
            autofocus: true,
            onChanged: (value) {
              name = value;
            }),
        ExpandingButton(
            image: "resources/icons/save.png",
            height: 20,
            color: DarkGreen,
            onPressed: () {
              Navigator.pop(context, name);
            })
      ],
    );
  }
}

class LoadGameDialog extends StatelessWidget {
  List<String> _allGameNames;

  LoadGameDialog({List<String> allGameNames}) : _allGameNames = allGameNames;

  @override
  Widget build(BuildContext context) {
    return PaddedDialog(
      title: AppLocalizations.of(context).tr('load_game'),
      children: <Widget>[
        Column(
            children: _allGameNames.map((name) {
          return GestureDetector(
              child: Text(name, style: TextStyle(fontSize: 30, height: 1.5)),
              onTap: () {
                Navigator.pop(context, name);
              });
        }).toList())
      ],
    );
  }
}
