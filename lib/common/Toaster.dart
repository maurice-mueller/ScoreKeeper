import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Toaster {
  static showSuccess({String i18n, List<String> args, BuildContext context}) {
    Fluttertoast.cancel();
    Fluttertoast.showToast(msg: AppLocalizations.of(context).tr(i18n, args: args));
  }

  static showProcess({String i18n, List<String> args, BuildContext context}) {
    Fluttertoast.cancel();
    Fluttertoast.showToast(msg: AppLocalizations.of(context).tr(i18n, args: args));
  }
}