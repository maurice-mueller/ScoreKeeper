import 'package:scoped_model/scoped_model.dart';

class GameMetaModel extends Model {
  String _name;
  GameMetaModel({String name}): _name = name;

  String get name => _name;

  set name(String value) =>
      _withNotify(() {
        _name = value;
      });

  void _withNotify(var f) {
    f();
    notifyListeners();
  }
}
