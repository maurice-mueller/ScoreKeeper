import 'package:flutter/widgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:score_keeper/common/GameMetaModel.dart';
import 'package:score_keeper/common/RouteArguments.dart';
import 'package:score_keeper/menu/AboutScreen.dart';
import 'package:score_keeper/menu/AppSettingScreen.dart';
import 'package:score_keeper/menu/HomeScreen.dart';
import 'package:score_keeper/simple_game/SimpleGameModel.dart';
import 'package:score_keeper/simple_game/SimpleGameView.dart';

class Routes {
  static const String HOME_SCREEN = "/";
  static const String SIMPLE_GAME_SCREEN = "/simple_game";
  static const String SETTINGS_SCREEN = "/settings";
  static const String ABOUT_SCREEN = "/about";

  static Map<String, WidgetBuilder> asMap() {
    return {
      HOME_SCREEN: (context) => HomeScreen(),
      SIMPLE_GAME_SCREEN: (context) => ScopedModel<GameMetaModel>(
          model: (ModalRoute.of(context).settings.arguments as RouteArguments).metaModel,
          child: ScopedModel<SimpleGameModel>(
              model:
              (ModalRoute.of(context).settings.arguments as RouteArguments).model as SimpleGameModel,
              child: SimpleGameView())),
      SETTINGS_SCREEN: (context) => AppSettingScreen(),
      ABOUT_SCREEN: (context) => AboutScreen()
    };
  }
}
