import 'package:flutter_test/flutter_test.dart';
import 'package:score_keeper/simple_game/SimpleGameModel.dart';

void main() {
  test('Add player to model', () {
    SimpleGameModel model = SimpleGameModel();
    expect(model.currentState.players.length, 0);
    model.addPlayer(SimplePlayer.createNew('test'));
    expect(model.currentState.players.length, 1);
  });
  test('remove player from model', () {
    SimpleGameModel model = SimpleGameModel();
    SimplePlayer player = SimplePlayer.createNew('test');
    model.addPlayer(player);
    model.removePlayer(player);
    expect(model.currentState.players.length, 0);
  });
  test('Add score to player', () {
    SimpleGameModel model = SimpleGameModel();
    SimplePlayer player = SimplePlayer.createNew('test');
    model.addPlayer(player);
    expect(model.currentState.players[0].scores.length, 0);

    model.addScoreToPlayer(player, 120);
    expect(model.currentState.players[0].scores.length, 1);
    expect(model.currentState.players[0].score, 120);

    model.addScoreToPlayer(player, -20);
    expect(model.currentState.players[0].scores.length, 2);
    expect(model.currentState.players[0].score, 100);
  });
}
